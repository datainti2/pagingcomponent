import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetComponent } './src/base-widget/base-widget.component';
export * from './src/base-widget/base-widget.component';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetComponent
  ],
  exports: [
    BaseWidgetComponent
  ]
})
export class pagingModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: pagingModule,
      providers: []
    };
  }
}
