import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import * as _ from 'underscore';

@Component({
  selector: 'app-paging',
  templateUrl: './base-widget.component.html',
  styleUrls: ['./base-widget.component.css']
})
export class BaseWidgetComponent implements OnInit {
  constructor(private http: Http) { }
    private allItems : any[];

    // pager object
    pager: any = {};

    // paged items
    pagedItems: any[];
    ngOnInit() {
        this.allItems  = [
    {
      "name" : "Sosial",
      "count" : "1.950",
      "img" : "assets/img/ic_image_black_48dp_1x.png"
    },
    {
      "name" : "Ekonomi",
      "count" : "2.560",
      "img" : "assets/img/ic_image_black_48dp_1x.png"
    },
    {
      "name" : "Politik",
      "count" : "1.203",
      "img" : "assets/img/ic_image_black_48dp_1x.png"
    },
    {
      "name" : "Gaya Hidup",
      "count" : "3.205",
      "img" : "assets/img/ic_image_black_48dp_1x.png"
    },
    {
      "name" : "Teknologi",
      "count" : "3.405",
      "img" : "assets/img/ic_image_black_48dp_1x.png"
    },
    {
      "name" : "Pendidikan",
      "count" : "1.506",
      "img" : "assets/img/ic_image_black_48dp_1x.png"
    },
    {
      "name" : "Sosial",
      "count" : "1.950",
      "img" : "assets/img/ic_image_black_48dp_1x.png"
    },
    {
      "name" : "Ekonomi",
      "count" : "2.560",
      "img" : "assets/img/ic_image_black_48dp_1x.png"
    },
    {
      "name" : "Politik",
      "count" : "1.203",
      "img" : "assets/img/ic_image_black_48dp_1x.png"
    },
    {
      "name" : "Gaya Hidup",
      "count" : "3.205",
      "img" : "assets/img/ic_image_black_48dp_1x.png"
    },
    {
      "name" : "Teknologi",
      "count" : "3.405",
      "img" : "assets/img/ic_image_black_48dp_1x.png"
    },
    {
      "name" : "Pendidikan",
      "count" : "1.506",
      "img" : "assets/img/ic_image_black_48dp_1x.png"
    }
        ];
        this.setPage(1);
    }

    getPager(totalItems: number, currentPage: number = 1, pageSize: number = 8) {
            // calculate total pages
            let totalPages = Math.ceil(totalItems / pageSize);

            let startPage: number, endPage: number;
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }

            // calculate start and end item indexes
            let startIndex = (currentPage - 1) * pageSize;
            let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

            // create an array of pages to ng-repeat in the pager control
            let pages = _.range(startPage, endPage + 1);

            // return object with all pager properties required by the view
            return {
                totalItems: totalItems,
                currentPage: currentPage,
                pageSize: pageSize,
                totalPages: totalPages,
                startPage: startPage,
                endPage: endPage,
                startIndex: startIndex,
                endIndex: endIndex,
                pages: pages
            };
        }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service
        this.pager = this.getPager(this.allItems.length, page);

        // get current page of items
        this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

}
